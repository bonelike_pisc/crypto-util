use std::fs::File;
use std::io::{Read, BufReader, Write};
use crypto_util::rc4::*;

#[test]
fn test_correct_key() {
    // 暗号化して書き出し
    let mut file = File::create("tests/data/crypted1.dat").unwrap();
    let raw_data = b"Hello, world !".to_vec();
    let encrypted = encrypt_with_rc4(raw_data, b"testkey0123").unwrap();
    file.write_all(encrypted.as_slice()).unwrap();

    // 中身を表示してみる
    println!("{:?}", encrypted);

    // 書き出したデータを再度読み込んで復号
    let mut source = File::open("tests/data/crypted1.dat")
        .map(|f| Rc4DecodingReader::new(BufReader::new(f), b"testkey0123"))
        .unwrap();
    let mut data = Vec::new();
    source.read_to_end(&mut data).unwrap();

    assert_eq!(data, b"Hello, world !");
}

#[test]
fn test_util() {
    write_file_encrypting(b"hoge123__fuga...".to_vec(), "tests/data/crypted2.dat", b"06789").unwrap();
    let data = read_file_decrypting("tests/data/crypted2.dat", b"06789").unwrap();
    assert_eq!(data, b"hoge123__fuga...");
}

#[test]
fn test_uncorrect_key() {
    write_file_encrypting(b"Hello world !".to_vec(), "tests/data/crypted3.dat", b"06789").unwrap();
    let data = read_file_decrypting("tests/data/crypted3.dat", b"06788").unwrap();
    // 暗号化前と復号後でデータが異なっていればOK
    println!("before: {:?}", b"Hello world !");
    println!("after: {:?}", data);
}
