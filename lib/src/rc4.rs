use std::io::{Read, Seek};
use std::sync::Mutex;
use crypto::rc4::Rc4;
use crypto::buffer::{RefReadBuffer, RefWriteBuffer};
use crypto::symmetriccipher::{Decryptor, Encryptor};

// RC4暗号アルゴリズムで復号しつつデータを読み込むReader
pub struct Rc4DecodingReader<'a, R> where R: Read {
    inner: R,
    rc4: Mutex<Rc4>,
    key: &'a [u8]
}

impl <'a, R> Rc4DecodingReader<'a, R> where R: Read {
    pub fn new(inner: R, key: &'a [u8]) -> Self {
        Self {
            inner: inner,
            rc4: Mutex::new(Rc4::new(key)),
            key: key
        }
    }

    fn decrypt(&mut self, data: &[u8]) -> Result<Vec<u8>, Box<dyn std::error::Error>> {
        let mut read_buf = RefReadBuffer::new(data);
        let mut tmp_buf = new_sized_vec_u8(data.len());
        let mut write_buf = RefWriteBuffer::new(tmp_buf.as_mut_slice());
        {
            // スレッドセーフになるようにrc4をロック
            let mut locked_rc4 = self.rc4.lock()
                .map_err(|_e| "mutex lock failed".to_string())?;
            (*locked_rc4).decrypt(
                &mut read_buf,
                &mut write_buf,
                true
            ).map_err(|_e| "failed to decrypt".to_string())?;
        }
        Ok(tmp_buf)
    }
}

impl <R> Read for Rc4DecodingReader<'_, R> where R: Read {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        let size = self.inner.read(buf)?;
        let decrypted = self.decrypt(&buf[0..size])
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;
        
        for i in 0..size {
            buf[i] = decrypted[i];
        }
        Ok(size)
    }
}

impl <R> Seek for Rc4DecodingReader<'_, R> where R: Read + Seek {
    fn seek(&mut self, pos: std::io::SeekFrom) -> std::io::Result<u64> {
        // rc4を初期化し、移動先オフセットまでの分だけ空回りさせる
        let from_start = self.inner.seek(pos)?;
        self.rc4 = Mutex::new(Rc4::new(self.key));
        let for_dryrun = new_sized_vec_u8(from_start as usize);
        self.decrypt(for_dryrun.as_slice())
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;
        Ok(from_start)
    }
}

// 暗号化は単にメモリ上で変換を行う
pub fn encrypt_with_rc4(data: Vec<u8>, key: &[u8]) -> Result<Vec<u8>, String> {
    let mut ret = new_sized_vec_u8(data.len());
    let mut read_buf = RefReadBuffer::new(data.as_slice());
    let mut write_buf = RefWriteBuffer::new(ret.as_mut_slice());
    Rc4::new(key).encrypt(
        &mut read_buf,
        &mut write_buf,
        true
    ).map_err(|_e| "failed to encrypt".to_string())?;
    Ok(ret)
}

fn new_sized_vec_u8(sz: usize) -> Vec<u8> {
    let mut ret = Vec::with_capacity(sz);
    for _i in 0..sz {
        ret.push(0);
    }
    ret
}

/* 以下、お便利メソッド */

use std::path::Path;
use std::fs::File;
use std::io::{BufReader, Write};

// ファイルからデータを読み込んで復号
pub fn read_file_decrypting<P: AsRef<Path>>(source: P, key: &[u8]) -> std::io::Result<Vec<u8>> {
    let mut reader = File::open(source)
        .map(|f| Rc4DecodingReader::new(BufReader::new(f), key))?;
    let mut ret = Vec::new();
    reader.read_to_end(&mut ret)?;
    Ok(ret)
}

// データを暗号化してファイルに書き出し
pub fn write_file_encrypting<P: AsRef<Path>>(data: Vec<u8>, dest: P, key: &[u8]) -> std::io::Result<()> {
    let mut file = File::create(dest)?;
    let encrypted = encrypt_with_rc4(data, key)
        .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))?;
    file.write_all(encrypted.as_slice())?;
    Ok(())
}
