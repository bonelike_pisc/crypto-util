use crypto_util::rc4;

/**
 * ファイルをRC4で暗号化/復号するコマンドラインツール
 * usage: 
 *  暗号化: cargo run -- -e -f [入力ファイルのパス] -t [出力ファイルのパス] -k [暗号キー]
 *  復号: cargo run -- -d -f [入力ファイルのパス] -t [出力ファイルのパス] -k [暗号キー]
 */
fn main() {
    let args = std::env::args();
    let config = parse_arg(args).unwrap();
    match config.mode {
        Mode::Encrypt => {
            let data = std::fs::read(&config.from_path).unwrap();
            rc4::write_file_encrypting(data, &config.to_path, &config.key).unwrap();
        },
        Mode::Decrypt => {
            let data = rc4::read_file_decrypting(&config.from_path, &config.key).unwrap();
            std::fs::write(&config.to_path, data).unwrap();
        },
    }
}

fn parse_arg(args: std::env::Args) -> Result<Config, String> {
    let mut state = ArgState::Normal;
    let mut mode = Option::None;
    let mut from_path = Option::None;
    let mut to_path = Option::None;
    let mut key = Option::None;
    for arg in args {
        //println!("{}", arg);
        match state {
            ArgState::Normal => {
                // get option
                match arg.as_str() {
                    "-f" | "--from" => { state = ArgState::From; },
                    "-t" | "--to" => { state = ArgState::To; },
                    "-k" | "--key" => { state = ArgState::Key; },
                    "-e" | "--encrypt" => { mode = Some(Mode::Encrypt); },
                    "-d" | "--decrypt" => { mode = Some(Mode::Decrypt); },
                    _ => { /*panic!("invalid option {}", arg);*/ }
                }
            },
            ArgState::From => {
                from_path = Some(arg);
                state = ArgState::Normal;
            },
            ArgState::To => {
                to_path = Some(arg);
                state = ArgState::Normal;
            },
            ArgState::Key => {
                key = Some(arg.into_bytes());
                state = ArgState::Normal;
            }
        }
    }
    /*
    println!("mode:{}, from:{}, to:{}, key:{}",
        match mode { 
            Some(Mode::Encrypt) => "Encrypt",
            Some(Mode::Decrypt) => "Decrypt",
            _ => "NotDefined",
        }.to_string(),
        from_path.unwrap_or("None".to_string()),
        to_path.unwrap_or("None".to_string()),
        String::from_utf8(key.unwrap_or(b"None".to_vec())).unwrap()
    );*/

    Ok(Config {
        mode: mode.ok_or("mode is not specified.")?,
        from_path: from_path.ok_or("from_path is not specified.")?,
        to_path: to_path.ok_or("to_path is not specified.")?,
        key: key.ok_or("key is not specified.")?,
    })

}

enum ArgState {
    Normal,
    From,
    To,
    Key,
}

enum Mode {
    Encrypt,
    Decrypt,
}

struct Config {
    pub mode: Mode,
    pub from_path: String,
    pub to_path: String,
    pub key: Vec<u8>
}
